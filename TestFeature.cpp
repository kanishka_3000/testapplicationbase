/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include <gtest/gtest.h>
#include <feature/StringDataProcessor.h>
class MockStringProcessingSource : public feature::StringDataProcessorDataSource
{
public:
    MockStringProcessingSource(QMap<QString, QVariant>& mapData):
        map_data(mapData){}
    // StringDataProcessorDataSource interface
public:
    virtual QMap<QString, QVariant> &GetData() const
    {
        return map_data;
    }
    QMap<QString, QVariant>& map_data;
};

class TestFeature: public ::testing::Test
{
public:
    void SetUp()
    {

    }
    void TearDown()
    {

    }
};


TEST_F(TestFeature, BasicCheck)
{
    QMap<QString, QVariant> mapdata ;
    mapdata.insert("date", "2015-6-5");
    mapdata.insert("final", "japan");
    MockStringProcessingSource mockData(mapdata);
    feature::StringDataProcessor oStringProces;
    QString sData("this is the@finaltest without @date in side the total test");
    oStringProces.Bind(mockData);
    oStringProces.ProcessString(sData);
    EXPECT_STREQ(qPrintable(sData), "this is thejapantest without 2015-6-5 in side the total test");
   // QCOMPARE(sData , QString("this is thejapantest without 2015-6-5 in side the total test"));
}


