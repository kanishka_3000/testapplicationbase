/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef MOCKLOGGER
#define MOCKLOGGER
#include <Logger.h>
#include <gmock/gmock.h>
class MockLogger: public Logger
{


    // Logger interface
public:
    MOCK_METHOD0(OpenLogFile, bool ());
    MOCK_METHOD1(ClearLogs,int (int iPastDays));
    MOCK_METHOD1(WriteLog, void (QString sLogString));

};

#endif // MOCKLOGGER

