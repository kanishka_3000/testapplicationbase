/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include <gtest/gtest.h>
#include <core/TableConfigHandler.h>
#include <Util.h>
#include <QDir>
#include <core/MockLogger.h>
#include <QDebug>
#include <algorithm>
#include <tuple>
class TestTableConfigHandler: public ::testing::Test
{

public:
    static void SetUpTestCase()
    {
        QFile oFile(":/test/Resources/TableConfig.xml");
        bool bOpen = oFile.open(QIODevice::ReadOnly);

        QString sHome = QDir::homePath();
        s_FilePath = QString("%1/file_%2.xml").arg(sHome).arg(Util::GetUnique());
        bOpen = oFile.copy(s_FilePath);

        oFile.close();

    }
    static void TearDownTestCase()
    {
        QFile oFile(s_FilePath);
        oFile.open(QIODevice::ReadWrite);
        oFile.remove();
    }

    void SetUp()
    {

    }
    void TearDown()
    {

    }
protected:
    bool ValidateFieldSet(std::list<std::tuple<std::string, std::string, bool> >expectedVals,
                          std::map<std::string, std::shared_ptr<FieldConfig> > &maprealval)
    {
        bool bReturn = true;
        for(auto itr = expectedVals.begin(); itr != expectedVals.end(); itr++)
        {
            std::tuple<std::string, std::string,bool> field = *itr;
            std::string sFieldName = std::get<0>(field);
            std::string sDisplayName = std::get<1>(field);
            bool bIsVisible = std::get<2>(field);

            std::map<std::string, std::shared_ptr<FieldConfig> >::iterator itrreal =
                    maprealval.find(sFieldName);
            std::shared_ptr<FieldConfig> fieldval = itrreal->second;
            if(!(fieldval->s_FiledName == sFieldName.c_str()  &&
                    fieldval->s_DisplayName == sDisplayName.c_str()
                    && bIsVisible == fieldval->b_IsVisibl))
            {
               bReturn = false;
            }

        }
        return bReturn;
    }

    MockLogger  o_Logger;
    static QString s_FilePath;
};
QString TestTableConfigHandler::s_FilePath = "";
TEST_F(TestTableConfigHandler, initializationCheck)
{
    TableConfigHandler oConfHandler;
    oConfHandler.SetLogger(&o_Logger);
    std::vector<QString> vecOutput;
    EXPECT_CALL(o_Logger, WriteLog(::testing::_)).
            Times(2).
            WillRepeatedly(::testing::Invoke(
             [&](QString sLogVal)
             {
                               vecOutput.push_back(sLogVal);
             }
                               ));


    oConfHandler.Initialize(s_FilePath);

    //Checking for initialization callbacks
    EXPECT_TRUE(std::any_of(vecOutput.begin(), vecOutput.end(),
                            [&](QString svalue)
    {
                    return (svalue == "Window Configs Manage Users");
    }
                ));

    //Checking internal values
    WindowConfig oConfig = oConfHandler.GetWindowConfig("Manage Users");
    ASSERT_EQ(oConfig.GetFieldCount(), 3);//There are only 3 visible 3 items in sample

    std::map<std::string, std::shared_ptr<FieldConfig> > mapFieldConfigs;
    for(int i = 0; i < 4; i++)
    {
        mapFieldConfigs.insert(std::pair<std::string, std::shared_ptr<FieldConfig>>(
                                   qPrintable(oConfig.GetFieldConfig(i)->s_FiledName)
                                   ,oConfig.GetFieldConfig(i)));
    }
    bool bval = ValidateFieldSet(
    {
                    std::make_tuple("username","User Name",true),
                    std::make_tuple("name","Name",true),
                    std::make_tuple("rolename", "Role", true)
    }, mapFieldConfigs
                );

    EXPECT_TRUE(bval);
}
