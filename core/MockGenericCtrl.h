/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/
#include <gmock/gmock.h>
#include <GenericCtrl.h>
#ifndef MOCKGENERICCTRL
#define MOCKGENERICCTRL

class MockGenericCtrl: public GenericCtrl
{
public:
    MockGenericCtrl(QWidget* parent):
        GenericCtrl(parent){}
    MOCK_METHOD1(OnPreCreateWnd, void (GenericWnd* pParent));
    MOCK_METHOD0(OnCreateCtrl, void ());
    MOCK_METHOD0(OnPostCreateCtrl,void () );
    MOCK_METHOD1(OnKey, void (QString sKey));

};

#endif // MOCKGENERICCTRL

