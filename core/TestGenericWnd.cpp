/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/
#include <gtest/gtest.h>
#include <GenericWnd.h>
#include <core/MockApplication.h>
#include <core/MockGenericCtrl.h>
class TestGenericWnd: public ::testing::Test
{
public:
    static void SetUpTestCase()
    {

    }
    static void TearDownTestCase()
    {

    }
    void SetUp()
    {
        p_Wnd = new GenericWnd(nullptr, "teswind");
        p_GenCtrl = new MockGenericCtrl(p_Wnd);
    }


    void TearDown()
    {
        //delete p_GenCtrl;
       // delete p_Wnd;
    }
protected:
    MockApplication o_Application;

    GenericWnd* p_Wnd;
    MockGenericCtrl* p_GenCtrl;


    QSettings o_TestSettings;
    ActionStore o_ActionStore;

};

TEST_F(TestGenericWnd, preCreateWndCheck)
{


    //Check window initialization calls
    EXPECT_CALL(o_Application, GetWindowSettings()).
            Times(1).
            WillOnce(::testing::Return(&o_TestSettings));

    EXPECT_CALL(o_Application, GetActionStore()).
            Times(1).
            WillOnce(::testing::Return(&o_ActionStore));

   // EXPECT_CALL(o_Application, OnWindowDistroyed(p_Wnd)).
   //         Times(1);

    //Check Generic Ctrl Initialization calls
    EXPECT_CALL(o_Application, GetLogger()).
            Times(1).
            WillRepeatedly(::testing::Return(nullptr));//Called from Generic Wnd
    //OnPrecreateWnd

    EXPECT_CALL(*p_GenCtrl, OnCreateCtrl()).
            Times(1);
    EXPECT_CALL((*p_GenCtrl), OnPostCreateCtrl()).
            Times(1);

    p_Wnd->OnPreCreate(&o_Application);

}
TEST_F(TestGenericWnd, distributeKeyToChildrenCheck)
{
    EXPECT_CALL(*p_GenCtrl, OnKey(QString("TEST_Key"))).
            Times(1);
    p_Wnd->DistributeKeytoChildren("TEST_Key");

}
