/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef MOCKAPPLICATION
#define MOCKAPPLICATION
#include <Application.h>
#include <gmock/gmock.h>

class MockApplication: public Application
{
public:

     MOCK_METHOD2(Initialize, void (QApplication* pQtApplication, ObjectFactory *pFactory));

     MOCK_METHOD1(CreateWnd, GenericWnd* (int iWndID));
     MOCK_METHOD0(GetEntityFactory, EntityFactory*  ());
     MOCK_METHOD1(ChangePassword, void (QString sNewPassword));
     MOCK_METHOD0(GetUser, std::shared_ptr<User> ());
     MOCK_METHOD0(GetUserName, QString ());
     MOCK_METHOD2(AddDef, void (QString sDef, EntityDef* pDef));
     MOCK_METHOD1(SetStyleSheet, void (QString sFileName) );
     MOCK_METHOD1(GetConfigData, QVariant (QString sKey));
     MOCK_METHOD0(GetConfig, QSettings* ());

     MOCK_METHOD0(GetWindowSettings, QSettings* ());
     MOCK_METHOD0(GetEnumHandler, EnumHandler* ());
     MOCK_METHOD0(GetLogger, Logger* ());
     MOCK_METHOD0(GetTableConfigHander, TableConfigHandler* ());

     MOCK_METHOD0(GetActionStore, ActionStore* ());
     MOCK_METHOD0(GetTableStore,TableStore* ());
     MOCK_METHOD0(GetQApplication,QApplication* () );
     MOCK_METHOD1(OnWindowDistroyed, void (GenericWnd* pWnd));
     MOCK_CONST_METHOD0(GetObjectFactory, ObjectFactory *());
     MOCK_METHOD0(ResetData, void ());
     MOCK_METHOD0(GetLoginIcon,QString ());
     MOCK_METHOD0(GetApplicationName, QString ());
     MOCK_METHOD0(GetVersion, QString ());
     MOCK_METHOD1(FillCustomEnums, void (EnumHandler& rEnumHander));
     MOCK_METHOD0(CreateSetupManager, void ());

protected:
     MOCK_METHOD2(AddWindowCreator, void (int iWindowID, BaseWindowCreator* pWndCreator));


};

#endif // MOCKAPPLICATION

