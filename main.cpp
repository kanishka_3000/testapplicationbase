/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include <QApplication>
#include <QtTest>


#include <gtest/gtest.h>
#include <testcore/GtestRunner.h>
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

//    TestFeature ofeature;
//    QTest::qExec(&ofeature, 0, nullptr);

//    CSVTest oExporter;
//    QTest::qExec(&oExporter, 0, nullptr);
    testcore::GtestRunner oGtestRunner(argc, argv,a);
    oGtestRunner.RunTests();
    return a.exec();
}
