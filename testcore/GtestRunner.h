/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef GTESTRUNNER_H
#define GTESTRUNNER_H

#include <QObject>
#include <QApplication>
#include <gtest/gtest.h>
namespace testcore{
class GtestRunner:public QObject
{
    Q_OBJECT
public:
    GtestRunner(int argv, char** args, QApplication &rApp);
    void RunTests();

public slots:
    void RunningTests();
private:
    int i_Argv;
    char** az_Args;
    QApplication& r_App;
};
}
#endif // GTESTRUNNER_H
