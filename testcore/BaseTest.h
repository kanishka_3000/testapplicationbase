/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef BASETEST_H
#define BASETEST_H

#include <QObject>
#include <QtTest/QtTest>
#include <QTest>

namespace testcore {
class BaseTestSummerizer;
class BaseTest : public QObject
{
    Q_OBJECT
public:
    explicit BaseTest(BaseTestSummerizer& rSummerizer, QObject *parent = 0);


signals:
    void NotifyTestStatus(QString sTestName, int iStatus);
public slots:
     void StartTests();
};
}
#endif // BASETEST_H
