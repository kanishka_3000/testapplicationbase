/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef BASETESTSUMMERIZER_H
#define BASETESTSUMMERIZER_H

#include <QObject>
#include <testcore/BaseTest.h>
#include <QList>
namespace testcore{
class BaseTestSummerizer : public QObject
{
    Q_OBJECT
public:
    explicit BaseTestSummerizer(QApplication& parent );
    void Register(testcore::BaseTest* pTesCase);
    void StartTests();
signals:

public slots:
    void OnTestComplete(QString sTestName, int iStatus);

private:
    void OnSummeryPrint();
    void _Start();
    int i_TotalTestCount;
    int i_CurrentTestNumber;
    QList<QString> lst_FailedTests;
    QList<QString> lst_PassedTests;

    QSet<testcore::BaseTest*> set_AllTests;

    QApplication& p_Application;
};
}
#endif // BASETESTSUMMERIZER_H
