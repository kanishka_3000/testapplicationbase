/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "GtestRunner.h"
#include <QTimer>
testcore::GtestRunner::GtestRunner(int argv, char **args, QApplication& rApp):
    i_Argv(argv), az_Args(args),r_App(rApp),QObject(nullptr)
{

}

void testcore::GtestRunner::RunTests()
{
    QTimer::singleShot(1000, this, SLOT(RunningTests()));
}

void testcore::GtestRunner::RunningTests()
{
    ::testing::InitGoogleTest(&i_Argv, az_Args);
    int iResult = RUN_ALL_TESTS();
    r_App.exit(iResult);
}

