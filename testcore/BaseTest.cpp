/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "BaseTest.h"
#include <testcore/BaseTestSummerizer.h>
namespace  testcore {


BaseTest::BaseTest(BaseTestSummerizer &rSummerizer, QObject *parent) : QObject(parent)
{
    rSummerizer.Register(this);
}

void BaseTest::StartTests()
{
    int iStatus=  QTest::qExec(this,0, nullptr);
    NotifyTestStatus(metaObject()->className(), iStatus);
}

}
