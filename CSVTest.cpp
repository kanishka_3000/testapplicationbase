/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include <gtest/gtest.h>
#include <QDir>
#include <feature/CSVExporter.h>
class TestRow: public feature::CSVExporter::CSVDataRow
{
public:


    // CSVDataRow interface
public:
    virtual QString GetData(int iColumn);
};

class TestSource: public feature::CSVExporter::CSVDataSource
{
public:



    // CSVDataSource interface
public:
    virtual QStringList GetHeader() const;
    virtual int GetItemCount() const;
    virtual std::shared_ptr<feature::CSVExporter::CSVDataRow> GetDataRow(int iRow) const;
};

class CSVTest: public ::testing::Test
{

public:
    CSVTest();
    void SetUp()
    {

    }
    void TearDown()
    {

    }
};
CSVTest::CSVTest()
{

}
TEST_F(CSVTest, BaseTestCheck)
{
    feature::CSVExporter oCSVExp;
    QString sPath = QDir::currentPath();
    QString sFileName = "FDNF";
    TestSource oSource;
    oCSVExp.Init(&oSource);

    QString sPath2 = oCSVExp.Export(sPath, sFileName);
}





QString TestRow::GetData(int iColumn)
{
    return QString("row%1").arg(iColumn);
}

QStringList TestSource::GetHeader() const
{
    QStringList oList = {"col1", "col2","col3"};
    return oList;

}

int TestSource::GetItemCount() const
{
    return 3;
}

std::shared_ptr<feature::CSVExporter::CSVDataRow> TestSource::GetDataRow(int iRow) const
{
    std::shared_ptr<feature::CSVExporter::CSVDataRow> pRow(new TestRow());
    return pRow;
}
