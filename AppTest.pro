#-------------------------------------------------
#
# Project created by QtCreator 2016-06-16T20:29:25
#
#-------------------------------------------------

QT       += core testlib

QT       -= gui

TARGET = AppTest
CONFIG   += console
CONFIG   -= app_bundle
INCLUDEPATH += "../ApplicationBase"
DEPENDPATH += "../ApplicationBase"
INCLUDEPATH += entity

include(../ApplicationBase/ApplicationBase.pri)
TEMPLATE = app

LIBS += -L/usr/local/lib -lgtest -lgmock

SOURCES += main.cpp \
    $$PWD/TestFeature.cpp \
    $$PWD/CSVTest.cpp \
    $$PWD/testcore/BaseTest.cpp \
    $$PWD/testcore/BaseTestSummerizer.cpp \
    core/TestGenericWnd.cpp \
    testcore/GtestRunner.cpp \
    core/TestTableConfigHandler.cpp


HEADERS += \
    $$PWD/testcore/BaseTest.h \
    $$PWD/testcore/BaseTestSummerizer.h \
    testcore/GtestRunner.h \
    core/MockApplication.h \
    core/MockGenericCtrl.h \
    core/MockLogger.h

RESOURCES += \
    AppTest.qrc
